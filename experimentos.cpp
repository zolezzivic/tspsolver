#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <bits/stdc++.h>
#include "grafo.h"
#include "types.h"
#include "chrono"
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <chrono>
#include "hamiltonMasCercano.h"
#include "hamiltonMenorCosto.h"
#include "hamiltonAGM.h"
#include "hamiltonTabuMem.h"
#include "hamiltonTabuMemEstructura.h"

using namespace std;


void escribirResultado(string path, hamiltonianoOut res,  int64_t tiempo, bool vaciar){
    //Sacamos el txt de la extension
    path.pop_back();
    path.pop_back();
    path.pop_back();
    path += "res";
    printf("Creando el grafo %s\n", path.c_str());
    if(vaciar){
        ofstream fout1(path, ios::out);
        fout1.close();
    }

    ofstream fout2(path, ios::app);
    fout2 << res.cantNodos << "," << res.pesoTotal << "," << tiempo  << "," << caminoToString(res.nodosVisitados)  << endl;
    fout2.close();
}

//Este sirve para las euristicas comunes
void escribirTiempo(const GrafoMA &G, hamiltonianoOut (*algoritmo)(const GrafoMA&, const Nodo&) , string outPath, size_t iteraciones, size_t nodoInicial){

    int64_t tiempoTotal = 0;
    hamiltonianoOut res(0,vector<size_t>(), 0);

    for(size_t i = 0; i < iteraciones; i++){
        auto start = chrono::steady_clock::now();
        res = algoritmo(G, nodoInicial);
        auto end = chrono::steady_clock::now();
        tiempoTotal += (int64_t)(chrono::duration_cast<chrono::microseconds>(end - start).count()/iteraciones);
    }
    
    escribirResultado(outPath, res, tiempoTotal, algoritmo==HamiltonMasCercano::getHamilton);

}

//Este sirve para las metaheuristicas
//Grafo, Vinicial, Porcentaje, Memoria, Iteraciones
void escribirTiempo(const GrafoMA &G, hamiltonianoOut (*algoritmo)(const GrafoMA&, const Nodo&, size_t, size_t, size_t) , string outPath, size_t iteraciones, size_t nodoInicial){

    int64_t tiempoTotal = 0;
    hamiltonianoOut res(0,vector<size_t>(), 0);

    for(size_t i = 0; i < iteraciones; i++){
        auto start = chrono::steady_clock::now();
        res = algoritmo(G, nodoInicial, 5, 100, 1000);
        // res = algoritmo(G, nodoInicial, 100, 100, 500);
        auto end = chrono::steady_clock::now();
        tiempoTotal += (int64_t)(chrono::duration_cast<chrono::microseconds>(end - start).count()/iteraciones);
    }
    
    escribirResultado(outPath, res, tiempoTotal, false);

}


void tabuCompleto(const GrafoMA &G, hamiltonianoOut (*algoritmo)(const GrafoMA&, const Nodo&, size_t, size_t, size_t, string), string outPath, size_t iteraciones,string extension){
    //Sacamos el txt de la extension
    outPath.pop_back();
    outPath.pop_back();
    outPath.pop_back();
    outPath += extension;
    printf("Creando el archivo %s\n", outPath.c_str());
    ofstream fout(outPath, ios::out);
    fout << "Peso,PorcentajeVecindad,TamanioMemoria,Iteracion,Tiempo\n";

    fout.close();

    // algoritmo(G, 0, 100, 1000, 1000, outPath);

    // vector<size_t> vecindades = {5,15,25,50,75,100};
    // vector<size_t> memorias = {10,50,100,500,1000};

    // for(size_t vecindad : vecindades){
    //     printf("Vamos con la vecindad %ld\n", vecindad);
    //     algoritmo(G, 0, vecindad, memorias[2], 1000, outPath);
    // }

    // for(size_t memoria : memorias){
    //     if(memorias[2] == memoria) continue;
    //     printf("Vamos con la memoria %ld\n", memoria);
    //     algoritmo(G, 0, vecindades[2], memoria, 1000, outPath);
    // }
}
//Grafo, Vinicial, Porcentaje, Memoria, Iteraciones
void analisisMemoria(const GrafoMA &G, hamiltonianoOut (*algoritmo)(const GrafoMA&, const Nodo&, size_t, size_t, size_t), string outPath,string extension){
    //Sacamos el txt de la extension
    outPath.pop_back();
    outPath.pop_back();
    outPath.pop_back();

    string pathTime = "";
    if(extension == "mem") pathTime = outPath + "timeMem";
    if(extension == "est") pathTime = outPath + "timeEst";
    if(pathTime == "") exit(EXIT_FAILURE);

    outPath += extension;
    printf("Creando el archivo %s\n", outPath.c_str());

    ofstream timeOut(pathTime, ios::out);
    ofstream fout(outPath, ios::out);

    fout<<"CantNodos,";
    timeOut<<"CantNodos,";

    vector<size_t> memorias = {10,50,100,500,1000};
    size_t N = memorias.size();
    for(size_t i = 0; i < N; i++){
        fout<<memorias[i];
        timeOut<<memorias[i];
        if(i < N-1){
            fout<<",";
            timeOut<<",";
        }else{
            fout<<"\n";
            timeOut<<"\n";
        }         
    }
    fout<<G.getCantNodos()<<",";
    timeOut<<G.getCantNodos()<<",";

    for(size_t i = 0; i < N; i++){
        auto start = chrono::steady_clock::now();
        hamiltonianoOut res = algoritmo(G, 0, 100, memorias[i], 300);
        auto end = chrono::steady_clock::now();

        fout<<res.pesoTotal;
        timeOut<<(int64_t)(chrono::duration_cast<chrono::milliseconds>(end - start).count());

        if(i < N-1){
            fout<<",";
            timeOut<<",";
        }else{
            fout<<"\n";
            timeOut<<"\n";
        }   
    }
    fout.close();
    timeOut.close();
}

//Grafo, Vinicial, Porcentaje, Memoria, Iteraciones
void analisisVecindad(const GrafoMA &G, hamiltonianoOut (*algoritmo)(const GrafoMA&, const Nodo&, size_t, size_t, size_t), string outPath,string extension, size_t tamanioMemoria){
    //Sacamos el txt de la extension
    outPath.pop_back();
    outPath.pop_back();
    outPath.pop_back();

    string pathTime = "";
    if(extension == "mem") pathTime = outPath + "timeMem";
    if(extension == "est") pathTime = outPath + "timeEst";
    if(pathTime == "") exit(EXIT_FAILURE);
    outPath += extension;
    printf("Creando el archivo %s\n", outPath.c_str());

    
    ofstream timeOut(pathTime, ios::out);

    ofstream fout(outPath, ios::out);

    fout<<"CantNodos,";
    timeOut<<"CantNodos,";

    vector<size_t> vecindades = {5,25,50,75,100};
    size_t N = vecindades.size();
    for(size_t i = 0; i < N; i++){
        fout<<vecindades[i];
        timeOut<<vecindades[i];
        if(i < N-1){
            fout<<",";
            timeOut<<",";
        }else{
            fout<<"\n";
            timeOut<<"\n";
        }         
    }
    fout<<G.getCantNodos()<<",";
    timeOut<<G.getCantNodos()<<",";

    for(size_t i = 0; i < N; i++){
        auto start = chrono::steady_clock::now();
        hamiltonianoOut res = algoritmo(G, 0, vecindades[i], tamanioMemoria, 300);
        auto end = chrono::steady_clock::now();

        fout<<res.pesoTotal;
        timeOut<<(int64_t)(chrono::duration_cast<chrono::milliseconds>(end - start).count());

        if(i < N-1){
            fout<<",";
            timeOut<<",";
        }else{
            fout<<"\n";
            timeOut<<"\n";
        }   
    }
    fout.close();
    timeOut.close();
}


int main(int argc, char *argv[]){
    srand(42);

    if(argc != 4){
        printf("Pasa dir lectura, dir escritura y cantIteraciones\n");
        return 1;
    }

    if(argc == 4){
        string csvPath = argv[1];
        string outPath = argv[2];
        size_t cantIteraciones = size_t(stoi(argv[3]));

        //Si el path viene con una / al final la saco
        if(csvPath[csvPath.size()-1] == '/'){
            csvPath.pop_back();
        }

        if(outPath[outPath.size()-1] == '/'){
            outPath.pop_back();
        }

        DIR *dir;
        struct dirent *ent;
        //Intento abrir el directorio y si puedo guardo la info en dir
        if((dir = opendir (csvPath.c_str())) == NULL){
            printf("No pude abrir la carpeta");
            return 1;
        }

        struct stat s;
            
        //Loopeo por todos los archivos del directorio
        while ((ent = readdir (dir)) != NULL) {
            string filePath = csvPath+"/"+ent->d_name;
            string outPathFile = outPath+"/"+ent->d_name;
            //Guardo la info del archivo en s y chequeo que no sea un directorio
            if( stat((filePath).c_str(),&s) == 0 && (s.st_mode & S_IFREG)){
                printf("\n======================================\n");
                printf ("Abrimos el archivo %s\n", filePath.c_str());
                GrafoMA G = GrafoMA(filePath);
                if(G.getCantNodos() > 500){
                    continue;
                    printf("Problemas %s", filePath.c_str());
                    exit(EXIT_FAILURE);
                }
                // printf("======================================\n");
                // printf("Corriendo goloso 1\n");
                // escribirTiempo(G, HamiltonMasCercano::getHamilton, outPathFile, cantIteraciones, 0);
                // printf("======================================\n");
                // printf("Corriendo goloso 2\n");
                // escribirTiempo(G, HamiltonMenorCosto::getHamilton, outPathFile, cantIteraciones, 0);
                // printf("======================================\n");
                // printf("Corriendo AGM\n");
                // escribirTiempo(G, HamiltonAGM::getHamilton, outPathFile, cantIteraciones, 0);
                // printf("======================================\n");
                // printf("Corriendo Tabu por estructura\n");
                // escribirTiempo(G, HamiltonTabuMemEstructura::getHamilton, outPathFile, cantIteraciones, 0);
                // printf("======================================\n");
                // printf("Corriendo Tabu por memoria\n");
                // escribirTiempo(G, HamiltonTabuMem::getHamilton, outPathFile, cantIteraciones, 0);
                // printf("======================================\n");
                // printf("Corriendo Tabu por estructura completo\n");
                // tabuCompleto(G, HamiltonTabuMemEstructura::getHamiltonExp, outPathFile, 0,"est");
                // printf("======================================\n");
                // printf("Corriendo Tabu por memoria completo\n");
                // tabuCompleto(G, HamiltonTabuMem::getHamiltonExp, outPathFile, 0, "mem");
                // printf("======================================\n");
                // printf("Corriendo analisis memoria tabu memoria\n");
                // analisisMemoria(G, HamiltonTabuMem::getHamilton, outPathFile, "mem");
                // printf("======================================\n");
                // printf("Corriendo analisis memoria tabu estructura\n");
                // analisisMemoria(G, HamiltonTabuMemEstructura::getHamilton, outPathFile, "est");
                // printf("======================================\n");

                size_t tamanioMemoria = cantIteraciones;
                printf("======================================\n");
                printf("Corriendo analisis vecindad tabu memoria\n");
                analisisVecindad(G, HamiltonTabuMem::getHamilton, outPathFile, "mem", tamanioMemoria);
                // printf("======================================\n");
                // printf("Corriendo analisis vecindad tabu estructura\n");
                // analisisVecindad(G, HamiltonTabuMemEstructura::getHamilton, outPathFile, "est", tamanioMemoria);
                // printf("======================================\n");

                // vector<size_t> memorias = {10,50,100,500,1000};
                // size_t N = memorias.size();
                // printf("======================================\n");
                // printf("Corriendo analisis memoria tabu memoria\n");
                // for(size_t memoria : memorias){
                //     cout<<memoria<<endl;
                //     auto start = chrono::steady_clock::now();
                //     hamiltonianoOut res = HamiltonTabuMem::getHamilton(G, 0, 100, memoria, 300);
                //     auto end = chrono::steady_clock::now();
                //     cout<<(int64_t)(chrono::duration_cast<chrono::microseconds>(end - start).count())<<endl;
                    
                // }
                // printf("======================================\n");
                // printf("Corriendo analisis memoria tabu estructura\n");
                // for(size_t memoria : memorias){
                //     cout<<memoria<<endl;
                //     auto start = chrono::steady_clock::now();
                //     hamiltonianoOut res = HamiltonTabuMemEstructura::getHamilton(G, 0, 100, memoria, 300);
                //     auto end = chrono::steady_clock::now();
                //     cout<<(int64_t)(chrono::duration_cast<chrono::microseconds>(end - start).count())<<endl;
                    
                // }
            }
        }

        //Cuando termino cierro el directorio
        closedir (dir);

        

        return 0;
    }

    
    return 0;
}
