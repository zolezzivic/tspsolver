# TSP solvers

The Travelling Salesman Problem is a well-known NP-hard problem in combinatorian optimization, and it is one of the most studied problems in this area.
In this project several TSP solvers are analysed and compared in order to determine which algorithm works better when we need to solve the TSP problem. It is important to recall that, since TSP is a NP-hard problem, there are not polynomial time known solutions for it. That is why the project focus on finding out which heuristic or metaheuristic can provide the best possible solution, even though there is no ensurance that this solution is the real one.

